package libao3

import "testing"

func TestBasicParse(t *testing.T) {
	result := parseFilenameFromDisposition("attachment; filename=\"Only the monsters we.html\"")
	if result != "Only the monsters we.html" {
		t.Errorf("Failed!")
	}
}

func TestExtraSpaces(t *testing.T) {
	result := parseFilenameFromDisposition(" attachment;  filename=\"Only the monsters we.html\" ")
	if result != "Only the monsters we.html" {
		t.Errorf("Failed!")
	}
}

func TestCaseInsensitive(t *testing.T) {
	result := parseFilenameFromDisposition("attaChment; filenAme=\"Only the monsters we.html\"")
	if result != "Only the monsters we.html" {
		t.Errorf("Failed!")
	}
}

func TestInternalQuotes(t *testing.T) {
	result := parseFilenameFromDisposition("attachment; filename=\"Only the \\\"monsters we.html\"")
	if result != "Only the \"monsters we.html" {
		t.Errorf("Failed!")
	}
}

func TestSanitizeAbsPath(t *testing.T) {
	result := parseFilenameFromDisposition("attachment; filename=\"/etc/Only the monsters we.html\"")
	if result != "Only the monsters we.html" {
		t.Errorf("Failed!")
	}
}

func TestSanitizeRelPaths(t *testing.T) {
	result := parseFilenameFromDisposition("attachment; filename=\"../foo/Only the monsters we.html\"")
	if result != "Only the monsters we.html" {
		t.Errorf("Failed!")
	}
}
