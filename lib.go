package libao3

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"path/filepath"
	s "strings"
	"time"

	"golang.org/x/net/context/ctxhttp"
	"golang.org/x/net/html"
	"golang.org/x/net/publicsuffix"
)

// AO3Connection wraps an HTTP session with the AO3 site.
type AO3Connection struct {
	Site   *url.URL
	Client *http.Client
	Jar    *cookiejar.Jar
}

// NewConnection creates a new AO3 connection
func NewConnection() (*AO3Connection, error) {
	result := new(AO3Connection)

	// Make cookie-jar
	jar, err := cookiejar.New(&cookiejar.Options{PublicSuffixList: publicsuffix.List})
	if err != nil {
		return nil, err
	}
	result.Jar = jar

	// Make client
	result.Client = &http.Client{Jar: jar}

	result.Site, _ = url.Parse("https://archiveofourown.org")

	return result, nil
}

// BookMarkInfo represents information about a single bookmark
type BookMarkInfo struct {
	Work string
	Date *time.Time
}

// Login establishes an AO3 login session
func (connection *AO3Connection) Login(user string, password string) error {
	return connection.LoginContext(context.Background(), user, password)
}

// LoginContext establishes an AO3 login session
func (connection *AO3Connection) LoginContext(ctx context.Context, user string, password string) error {
	loginURL := &(*connection.Site)
	loginURL.Path = "/users/login"
	var formValues = make(url.Values)

	formValues.Set("user[login]", user)
	formValues.Set("user[password]", password)

	// Scrape the CSRF token out of the front page
	token, err := connection.getAuthToken(ctx)
	if err != nil {
		return fmt.Errorf("Unable to get auth token: %s", err.Error())
	}
	formValues.Set("authenticity_token", token)

	// TODO - get errors back
	ctxhttp.PostForm(ctx, connection.Client, loginURL.String(), formValues)

	return nil
}

func (connection *AO3Connection) getAuthToken(ctx context.Context) (string, error) {
	resp, err := ctxhttp.Get(ctx, connection.Client, connection.Site.String())
	if err != nil {
		return "", fmt.Errorf("Error getting auth token: %s", err.Error())
	}
	defer resp.Body.Close()

	tokenizer := html.NewTokenizer(resp.Body)
	for {
		token := tokenizer.Next()
		if token == html.StartTagToken || token == html.SelfClosingTagToken {

			nameBytes, hasAttrs := tokenizer.TagName()
			tagName := string(nameBytes)

			if tagName != "input" || !hasAttrs {
				continue
			}

			attrs := parseAttributes(tokenizer)
			name, hasName := attrs["name"]

			if !hasName || name != "authenticity_token" {
				continue
			}

			value, hasValue := attrs["value"]
			if !hasValue {
				return "", errors.New("Auth token value not found")
			}

			return value, nil

		} else if token == html.ErrorToken {
			break
		}
	}

	return "", errors.New("Auth token not found in index")
}

// GetBookmarks returns a user's bookmarks. If the current connection is not
// logged in private bookmarks will not be returned.
func (connection *AO3Connection) GetBookmarks(user string) ([]*BookMarkInfo, error) {
	return connection.GetBookmarksContext(context.Background(), user)
}

// GetBookmarksContext returns a user's bookmarks. If the current connection is not
// logged in private bookmarks will not be returned.
func (connection *AO3Connection) GetBookmarksContext(ctx context.Context, user string) ([]*BookMarkInfo, error) {
	var results []*BookMarkInfo

	if user == "" {
		return nil, fmt.Errorf("'user' should not be null")
	}
	if s.Contains(user, "/") {
		return nil, fmt.Errorf("'user' contains invalid characters")
	}

	path := fmt.Sprintf("/users/%s/bookmarks", user)

	log.Printf("Getting from '%s'\n", path)

	url := &(*connection.Site)
	url.Path = path

	for {

		someResults, hasNext, nextPath := getBookmarksOnce(ctx, connection.Client, url.String())

		results = append(results, someResults...)
		if !hasNext {
			break
		}
		// Append the found path to the existing base url
		nextURL, err := url.Parse(nextPath)
		if err != nil {
			return nil, fmt.Errorf("Invalid 'next' link: %s", err.Error())
		}
		url = url.ResolveReference(nextURL)
	}

	return results, nil
}

func getBookmarksOnce(ctx context.Context, client *http.Client, url string) ([]*BookMarkInfo, bool, string) {
	log.Printf("Getting bookmarks from: %s", url)

	resp, err := ctxhttp.Get(ctx, client, url)
	if err != nil {
		return nil, false, ""
	}
	defer resp.Body.Close()

	return readBookmarks(resp.Body)
}

func readBookmarks(r io.Reader) ([]*BookMarkInfo, bool, string) {
	var results []*BookMarkInfo

	var current *BookMarkInfo
	liCounter := 0
	lookingForDateText := false

	nextPath := ""
	foundNextPath := false

	log.Printf("Starting html parsing ...")

	t := html.NewTokenizer(r)
	for {
		token := t.Next()
		if token == html.ErrorToken {
			return results, foundNextPath, nextPath
		}

		// We need to parse the bookmarking date
		// out of a text-node. We'll set the flag
		// `lookingforDateText` when we expect it
		// to show up
		if token == html.TextToken {
			if !lookingForDateText {
				continue
			}
			lookingForDateText = false
			possibleDateText := string(t.Text())

			// "Mon Jan 2 15:04:05 -0700 MST 2006"
			bookmarkDate, err := time.Parse("2 Jan 2006", possibleDateText)
			if err == nil && current != nil {
				current.Date = &bookmarkDate
			}
			continue
		}
		lookingForDateText = false

		if token == html.StartTagToken {

			tagBytes, hasAttrs := t.TagName()
			tagName := string(tagBytes)

			// First, see if this looks link a link
			// to the 'next' page of bookmarks
			if !foundNextPath && tagName == "a" && hasAttrs {
				attrs := parseAttributes(t)

				rel, hasRel := attrs["rel"]
				if !hasRel || rel != "next" {
					continue
				}

				href, hasHref := attrs["href"]
				if !hasHref {
					continue
				}
				nextPath = href
				foundNextPath = true

				continue
			}

			// If we're already in a bookmark list-element, track
			// any nested lists

			isLi := tagName == "li"

			if isLi && liCounter > 0 {
				liCounter++
			}

			// Check to see if this looks like the start of
			// a bookmark
			if isLi && hasAttrs && liCounter == 0 {
				attrs := parseAttributes(t)
				liID, hasID := attrs["id"]
				if hasID && s.HasPrefix(liID, "bookmark_") {
					liCounter++

					workID := s.TrimPrefix(liID, "bookmark_")
					if workID == "" {
						current = nil
					} else {
						current = new(BookMarkInfo)
						current.Work = workID
					}
					continue
				}
			}

			// Look for the para containing the bookmarked-on date
			isPara := tagName == "p"
			if isPara && liCounter > 0 && hasAttrs {
				attrs := parseAttributes(t)
				classStr, hasClass := attrs["class"]
				if hasClass && classStr == "datetime" {
					lookingForDateText = true
					continue
				}
			}
		} else if token == html.EndTagToken {
			tagBytes, _ := t.TagName()
			tagName := string(tagBytes)
			if tagName == "li" && liCounter > 0 {
				liCounter--

				if liCounter == 0 && current != nil && current.Date != nil {
					results = append(results, current)
					current = nil
				}
			}
		}
	}
}

func parseAttributes(t *html.Tokenizer) map[string]string {
	retVal := make(map[string]string)
	for {
		key, value, hasMore := t.TagAttr()
		retVal[string(key)] = string(value)
		if !hasMore {
			break
		}
	}
	return retVal
}

// WorkFormat describes the format desired for a specific work
type WorkFormat int

const (
	// WorkFormatAZW3 - Amazon Kindle
	WorkFormatAZW3 WorkFormat = iota + 1
	// WorkFormatEPUB - epub
	WorkFormatEPUB
	// WorkFormatMOBI - mobi
	WorkFormatMOBI
	// WorkFormatPDF - pdf
	WorkFormatPDF
	// WorkFormatHTML - html
	WorkFormatHTML
)

func (f WorkFormat) extension() (string, error) {
	switch f {
	case WorkFormatAZW3:
		return "azw3", nil
	case WorkFormatEPUB:
		return "epub", nil
	case WorkFormatHTML:
		return "html", nil
	case WorkFormatMOBI:
		return "mobi", nil
	case WorkFormatPDF:
		return "pdf", nil
	}
	return "", errors.New("Invalid work format")
}

// WorkRenditionResponse comprises the return-value of
// GetWorkRendition
type WorkRenditionResponse struct {
	Body     io.ReadCloser
	Filename string
}

// GetWorkRendition retrives the body of a specific work, in the specified format.
// If 'err' is non-nil the caller is responsible for calling `r.Body.Close()`
func (connection *AO3Connection) GetWorkRendition(ctx context.Context, work string, format WorkFormat) (r *WorkRenditionResponse, err error) {
	ext, err := format.extension()
	if err != nil {
		return
	}

	url := &(*connection.Site)
	url.Path = fmt.Sprintf("/downloads/%s/filename.%s", work, ext)

	// Get the document, validate the response looks reasonable
	resp, err := ctxhttp.Get(ctx, connection.Client, url.String())
	if err != nil {
		return
	}

	status := resp.StatusCode
	if status < 200 || status >= 300 {
		err = fmt.Errorf("Received status '%s' (%v)", resp.Status, status)
		resp.Body.Close()
		return
	}

	// Try and parse the suggested file-name from the response
	disposition := resp.Header.Get("Content-Disposition")
	filename := parseFilenameFromDisposition(disposition)

	if filename != "" && filepath.Ext(filename) == "" {
		filename = filename + "." + ext
	}

	return &WorkRenditionResponse{Filename: filename, Body: resp.Body}, nil
}

func parseFilenameFromDisposition(disposition string) string {
	remainder := s.TrimSpace(disposition)
	if !s.HasPrefix(s.ToLower(remainder), "attachment;") {
		return ""
	}

	remainder = remainder[11:]
	remainder = s.TrimSpace(remainder)

	if !s.HasPrefix(s.ToLower(remainder), "filename=") {
		return ""
	}

	remainder = remainder[9:]

	if remainder[0] == '"' {
		if remainder[len(remainder)-1] != '"' {
			return ""
		}
		remainder = remainder[1 : len(remainder)-1]
		remainder = s.Replace(remainder, "\\\"", "\"", -1)
	}

	if remainder == "" {
		return ""
	}

	// We think we've parsed the filename out
	// now we need to do some sanitization on it
	file := filepath.Base(remainder)
	file = s.TrimLeft(file, "."+string(filepath.Separator))

	return file
}
