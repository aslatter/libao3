module gitlab.com/aslatter/libao3

go 1.11

require (
	golang.org/x/net v0.0.0-20191209160850-c0dbc17a3553
	gopkg.in/yaml.v2 v2.2.7
)
