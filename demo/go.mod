module gitlab.com/aslatter/libao3/demo

require (
	gitlab.com/aslatter/libao3 v0.0.0
	gopkg.in/yaml.v2 v2.2.7
)

replace gitlab.com/aslatter/libao3 => ../

go 1.11
