package main

import (
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	"gitlab.com/aslatter/libao3"
	yaml "gopkg.in/yaml.v2"
)

func main() {
	loadConfig()
	con, _ := libao3.NewConnection()

	// Test getting bookmarks
	results, _ := con.GetBookmarks(appConfig.BookmarkUser)
	for _, bookmark := range results {
		fmt.Printf("Work: %s, Date: %s\n", bookmark.Work, bookmark.Date.String())
	}

	// Test reading a work
	fmt.Print("\n")
	demoWork := "16136192" // https://archiveofourown.org/works/16136192/chapters/37700990
	resp, err := con.GetWorkRendition(context.Background(), demoWork, libao3.WorkFormatHTML)
	if err != nil {
		fmt.Printf("Error retrieving work %#v: %s!\n", demoWork, err.Error())
	} else {
		length, _ := io.Copy(ioutil.Discard, resp.Body)
		fmt.Print("Downloaded a work!\n")
		fmt.Printf("Got: %#v\n", resp.Filename)
		fmt.Printf("Length: %v\n", length)
		resp.Body.Close()
	}
}

func loadConfig() {
	configPath := filepath.Join(filepath.Dir(os.Args[0]), "config.yaml")
	configBytes, err := ioutil.ReadFile(configPath)
	if err != nil {
		log.Fatalf("Unable to load config file from %s: %s", configPath, err.Error())
	}

	err = yaml.Unmarshal(configBytes, &appConfig)
	if err != nil {
		log.Fatalf("Error parsing app config: %s", err)
	}
}

type config struct {
	BookmarkUser string `yaml:"username"`
}

var appConfig = config{}
