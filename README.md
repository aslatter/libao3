# Building

To build, run `go build`

# Testing

A demo program is in the `demo` folder. To run the demo, copy the example
config file to `config.yaml`, and edit it to change the user to point
to.

To build the demo, in the demo folder execute `go build`. To run the demo
execute `./demo`.

The demo will print the public bookmarks of the configured user.
